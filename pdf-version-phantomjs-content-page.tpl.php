<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <base href="<?php echo $GLOBALS['base_url'] . base_path(); ?>" />
    <?php echo drupal_get_css(); ?>
    <?php echo drupal_get_js(); ?>
  </head>
  <body class="pdf-version">
    <?php echo $content; ?>
  </body>
</html>

