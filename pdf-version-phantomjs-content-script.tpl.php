var page = require('webpage').create();

<?php if (!empty($paper_size)): ?>
page.paperSize = <?php echo json_encode($paper_size); ?>;
<?php endif; ?>
<?php if (!empty($viewport_size)): ?>
page.viewportSize = <?php echo json_encode($viewport_size); ?>;
<?php endif; ?>

// For TypeKit suppport.
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7';
page.customHeaders = { 'Referer': <?php echo json_encode($url); ?> };

<?php echo $extra; ?>

// Set content.
page.setContent(<?php echo json_encode($content); ?>, <?php echo json_encode($url); ?>);

<?php if (empty($disable_load_finished)): ?>
// Wait until we're told we're ready before rendering.
page.onLoadFinished = function (data) {
  page.render('/dev/stdout', { format: 'pdf' });
  phantom.exit();
}
<?php endif; ?>
